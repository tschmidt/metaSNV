#!/bin/bash
#########################################
#  metaSNP v1.1 - Initiate New Project	#
#########################################
#
# Helper script to initiate a new project file structure.

# This code is part of the metagenomic SNP calling pipeline (metaSNP)
# Copyright (c) 2016 Robin Munch
# Licenced under the GNU General Public License (see LICENSE) 


# Variables
PROJECT_NAME="$1"

# Usage Message
display_usage(){
	echo >&2 ""
	echo >&2 "	Usage: $(basename $0) project_name"
	echo >&2 ""
}

required_parameter() {
        echo >&2 ""
        echo >&2 "ERROR: '$1' is a required parameter"
        display_usage
        exit 1
}


make_dir(){

        echo >&2 ""	
        echo >&2 "	Project name is available. New project directory created here: $PROJECT_NAME"
        echo >&2 ""
        mkdir -p $PROJECT_NAME/{cov,bestsplits,snpCaller,filtered/{pop,ind},distances}

}

file_exists(){
        echo  >&2 ""
		echo  >&2 "	ERROR: The directory '$PROJECT_NAME' exists. Please choose a unique project name."
        echo  >&2 ""
}

# Check required parameters 
[ -n "$PROJECT_NAME" ] || required_parameter "project_name"

# Make
if [ ! -d "$PROJECT_NAME" ] 
then
		make_dir
else	
		file_exists	
fi
exit

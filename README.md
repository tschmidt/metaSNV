MetaSNP, a metagenomic SNP calling pipeline
============================================== 


The metaSNP pipeline performs variant calling on aligned metagenomic samples and enables species delineation with sub-species resolution.


Download
======== 

Via Git:

    git clone git@git.embl.de:rmuench/metaSNP.git
    
or [download](https://git.embl.de/rmuench/metaSNP/repository/archive.zip?ref=master) a zip file of the repository.


Dependencies
============

* Boost-1.53.0 or above

* samtools-1.19 or above
 
* Python-2.7 or above


Setup & Compilation
===================
MetaSNP is mainly implemented in C/C++ and needs to be compiled.

I. Setup
--------

a)  Change the SAMTOOLS variable in the **SETUPFILE** to your path to samtools.
    If you don't have samtools, you can find it [here](http://samtools.sourceforge.net/):
    
            http://samtools.sourceforge.net/
            
a)  Change the HTSLIB variable in the **SETUPFILE** to your path to htslib.
    If you don't have htslib, check your samtools folder first or download it [here](http://www.htslib.org/):
    
            http://www.htslib.org/

b)  Change the BOOST variable in the **SETUPFILE** to your path to BOOST library.
    If you don't have boost, you can find boost [here](http://www.boost.org/users/download/): 
        
            http://www.boost.org/users/download/
            
c)  Setup your own **reference database** or acquire the database we use.
    In order to acquire our database run the provided script in the parent directory:
    
            usage: ./getRefDB.sh 

II. Compilation:
----------------

1)   run `make` in the parent directory of metaSNP to compile qaTools and the snpCaller.
        
            usage: make
            

III. Environmental Variables:
----------------
We assume the metaSNP parent directory, ``samtools`` and ``python`` in your system path variable. 
You can set this variable temporarily by runing the following commands and permanently by putting these line into your .bashrc: 
    
            export PATH=/path/2/metaSNP:$PATH
            export PATH=/path/2/python:$PATH
            export PATH=/path/2/samtools:$PATH

Note: Replace '/path/2' with the corresponding global path.


Workflow:
=========
## Required Files:
* **'all\_samples'**    = a list of all BAM files, one /path/2/sample.bam per line (avoid duplicates!)
* **'ref_db'**  = the reference database in fasta format (f.i. multi-sequence fasta)
* **'gen\_pos'**    = a list with start and end positions for each sequence in the reference (format: sequence_id    start   end)

## Optional Files:
* **'db\_ann'** = a gene annotation file for the reference database.

## 1. Initiate a new project

        usage: metaSNP_NEW project_dirname
    
Generates a structured results directory for your project.

## 2. Part I: Pre-processing [optional]
Note: Subsequent SNP filtering depends on these coverage estimations.
This part can be skipped if you only want to use the 'raw' SNP output and do not intend to balance the workload or if you already performed the pre-processing for the dataset.

### a) run metaSNP_COV

        usage: metaSNP_COV project_dir/ all_samples
                
The script generates a list of commandline jobs. 
Run each commandline jobs before proceeding with the next step. 

### b) run metaSNP_OPT
Helper script for workload balancing (reference genome splitting).

        usage: metaSNP_OPT project_dir/ genome_def nr_splits[int]
        
        Parameters
         Required:
            project_dir/    = the projects result directory
            genome_def FILE    = Contig ranges in BED format. (Fields: Contig_id, contigStart, contigEnd)

         Optional:
            nr_splits INT	= INT for job parallelization (range: 1-100) [10]

Note: This step requires an appropriate reference genome_definitions file (included in our database).


## 3. Part II: SNP calling
Note: Can be run as a single job, independently of the pre-processing or you use an already existing genome split.

### a) metaSNP_SNP

        usage: metaSNP_SNP project_dir/ all_samples ref_db [options]

        Parameters:
         Required:
                project_dir     DIR     = the project directory.
                all_samples     FILE    = a list of bam files, one file per line.
                ref_db          FILE    = the reference multi-sequence FASTA file used for the alignments.

         Optional:
                -a, db_ann      FILE    = database annotation.
                -l, splits/     DIR     = bestsplits DIR for job parallelization (pre-processing). 

Note: Alternatively, use the ``-l splits/`` option to call SNPs for specific species, contig regions (BED) or single positions (contig_id pos). Unlisted contigs/pos are skipped.

The script generates a list of commandline jobs. Submit each commandline to your GE or compute locally.

## 4. Part III: Post-Processing (Filtering & Analysis)

### a) Filtering:
Note: requires SNP calling (Part II) to be done!
Caution: Perform this step seperately for individual SNPs and population SNPs.

    usage: metaSNP_filtering.py

        positional arguments:
            perc_FILE               input file with horizontal genome (taxon) coverage (breadth) per sample (percentage covered)
            cov_FILE                input file with average genome (taxon) coverage
                                    (depth) per sample (average number reads per site)
            snp_FILE                input files from SNP calling
            all_samples             list of input BAM files, one per line
            output_dir/             output folder

        optional arguments:
            -h, --help              show this help message and exit
            -p PERC, --perc PERC    Coverage breadth: Horizontal coverage cutoff
                                    (percentage taxon covered) per sample (default: 40.0)
            -c COV, --cov COV       Coverage depth: Average vertical coverage cutoff per
                                    taxon, per sample (default: 5.0)
            -m MINSAMPLES, --minsamples MINSAMPLES
                                    Minimum number of sample required to pass the coverage
                                    cutoffs per genome (default: 2)
            -s SNPC, --snpc SNPC    FILTERING STEP II: SNP coverage cutoff (default: 5.0)
            -i SNPI, --snpi SNPI    FILTERING STEP II: SNP occurence (incidence) cutoff
                                    within samples_of_interest (default: 0.5)

### b) Analysis:
>   TODO: include R scripts for computing pairwise distances and visualization


Example Tutorial
================

## 1. Run the setup & compilation steps and download the provided reference database.

    $ getRefDB.sh

## 2. Go to the EXAMPLE directory and download the samples with the getSamplesScript.sh

    $ cd EXAMPLE
    $ ./getSamplesScript.sh

## 3. Initiate a new project in the parent directory

    $ metaSNP_New tutorial

## 4. Generate the 'all_samples' file

    $ find `pwd`/EXAMPLE/samples -name “*.bam” > tutorial/all_samples

## 5. Prepare and run the coverage estimation

    $ metaSNP_COV tutorial/ tutorial/all_samples > runCoverage
    $ bash runCoverage

## 6. Perform a work load balancing step for run time optimization.

    $ metaSNP_OPT tutorial/ db/Genomev9_definitions 5
    $ bash runCoverage

## 7. Prepare and run the SNP calling step

    $ metaSNP_SNP tutorial/ tutorial/all_samples db/RepGenomesv9.fna -a db/RefOrganismDB_v9_gene.clean -l tutorial/bestsplits/ > runSNPcall
    $ bash runSNPcall

## 8. Run the post processing / filtering steps
### a) Compute allele frequencies for each position that pass the given thresholds.

    $ metaSNP_filtering.py tutorial/tutorial.all_perc.tab tutorial/tutorial.all_cov.tab tutorial/snpCaller/called_SNPs.best_split_* tutorial/all_samples tutorial/filtered/pop/

### b) Compute pair-wise distances between samples on their SNP profiles and create a PCoA plot.
    
    TODO!


Basic usage (tools and scripts)
===============================
If you are interested in using the pipeline in a more manual way (for example the metaSNP caller stand alone) feel free to explore the src/ directory.
You will find scripts as well as the binaries for qaCompute and the metaSNP caller in their corresponding directories (src/qaCompute and src/snpCaller) post compilation.

metaSNP caller
--------------
Calls SNPs from samtools pileup format and generates two outputs.

    usage: ./snpCall [options] < stdin.mpileup > std.out.popSNPs

    Options:
        -f,     faidx indexed reference genome.
        -g,     gene annotation file.
        -i,     individual SNPs.

Note: Expecting samtools mpileup as standard input

### __Output__
1. Population SNPs (pSNPs): 
Population wide variants that occur with a frequency of 1 % at positions with at least 4x coverage.

2. Individual specific SNPs (iSNPs):
Non population variants, that occur with a frequency of 10 % at positions with at least 10x coverage.


[qaComput](https://github.com/CosteaPaul/qaTools)
-------------------------------------------------
Computes normal and span coverage from a bam/sam file. 
Also counts unmapped and sub-par quality reads.

### __Parameters:__
   m	    -	Compute median coverage for each contig/chromosome. 
   		Will make running a bit slower. Off by default.
   
   q [INT]  -   Quality threshold. Any read with a mapping quality under
                INT will be ignored when computing the coverage.
                
		NOTE: bwa outputs mapping quality 0 for reads that map with
		equal quality in multiple places. If you want to condier this,
		set q to 0.
		
   d        -   Print coverage histrogram over each individual contig/chromosome.
   	        These details will be printed in file <output>.detail
   	        
   p [INT]  -   Print coverage profile to bed file, averaged over given window size.  
   
   i        -   Silent run. Will not print running info to stdout.
    
   s [INT]  -   Compute span coverage. (Use for mate pair libs)
                Instead of actual read coverage, using the options will consider
                the entire span of the insert as a read, if insert size is
		lower than INT. 
 		For an accurate estimation of span coverage, I recommend
		setting an insert size limit INT around 3*std_dev of your lib's 
		insert size distribution.
    
   c [INT]  -   Maximum X coverage to consider in histogram.
    
   h [STR]  -   Use different header. 
                Because mappers sometimes break the headers or simply don't output them, 
		this is provieded as a non-kosher way around it. Use with care!
    
   For more info on the parameteres try ./qaCompute
   

metaSNP_filtering.py
--------------------   
usage: metaSNP filtering [-h] [-p PERC] [-c COV] [-m MINSAMPLES] [-s SNPC]
                         [-i SNPI] 
                         perc_FILE cov_FILE snp_FILE [snp_FILE ...]
                         all_samples output_dir/

metaSNP filtering

positional arguments:
  perc_FILE             input file with horizontal genome (taxon) coverage
                        (breadth) per sample (percentage covered)
  cov_FILE              input file with average genome (taxon) coverage
                        (depth) per sample (average number reads per site)
  snp_FILE              input files from SNP calling
  all_samples           list of input BAM files, one per line
  output_dir/           output folder

optional arguments:
  -h, --help            show this help message and exit
  -p PERC, --perc PERC  Coverage breadth: Horizontal coverage cutoff
                        (percentage taxon covered) per sample (default: 40.0)
  -c COV, --cov COV     Coverage depth: Average vertical coverage cutoff per
                        taxon, per sample (default: 5.0)
  -m MINSAMPLES, --minsamples MINSAMPLES
                        Minimum number of sample that have to pass the
                        filtering criteria in order to write an output for the
                        representative Genome (default: 2)
  -s SNPC, --snpc SNPC  FILTERING STEP II: SNP coverage cutoff (default: 5.0)
  -i SNPI, --snpi SNPI  FILTERING STEP II: SNP occurence (incidence) cutoff
                        within samples_of_interest (default: 0.5)



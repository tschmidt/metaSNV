import sys

#We assume that contings are sorted within the genome, so that a contig won't show up 
#randomly in the file, but together with its friends.

cov = open(sys.argv[1],'r')
xcov = open(sys.argv[2],'r')

genomeMap = dict()
#Drop cov header
cov.readline()

while True:
    covL = cov.readline()
    xcovL = xcov.readline()
    if not xcovL:
        break
    name = covL.split('\t')[0]
    namex = xcovL.split('\t')[0]
    if name != namex:
        print name
        print namex
        print 'oups'

    taxId = name.split('.')[0]
    if taxId not in genomeMap:
        genomeMap[taxId] = [0.0,0.0,0.0,0.0] #why not [0,0.0,0.0,0.0] since 1st is int?
    #Add the length
    genomeMap[taxId][0] += int(covL.split('\t')[1])
    #genomeMap[taxId][0] += 1
    #Add the average coverage weighted with the length
    genomeMap[taxId][1] += float(covL.split('\t')[2]) * int(covL.split('\t')[1])
    #genomeMap[taxId][1] += int(float(covL.split('\t')[2]))
    #Add the number of bases covered at 1x
    genomeMap[taxId][2] += int(xcovL.split('\t')[2])
    #Add the number of bases covered at 2x
    genomeMap[taxId][3] += int(xcovL.split('\t')[3])

outf = open(sys.argv[3],'w')
#Write header
outf.write('TaxId\tAverage_cov\tPercentage_1x\tPercentage_2x\n')

moreThan10x = 0
#Print the average coverage over the taxId's
for k in genomeMap.keys():
    if genomeMap[k][1]/genomeMap[k][0] > 10:
        moreThan10x += 1
    outf.write('%s\t%f\t%f\t%f\n'%(k,genomeMap[k][1]/genomeMap[k][0],genomeMap[k][2]/genomeMap[k][0]*100,genomeMap[k][3]/genomeMap[k][0]*100))

outf.close()
cov.close()
xcov.close()

#print moreThan10x
print 'Done'


import sys
import operator

cov = open(sys.argv[1],'r')
perc = open(sys.argv[2],'r')
genomes = open(sys.argv[3],'r')
nrToSplit = int(sys.argv[4])
outf = sys.argv[5]

genomeDict = dict()
contigDict = dict()

for line in genomes:
    genome = line.split('\t')[0].split('.')[0]
    leng = int(line.rstrip().split('\t')[2])
    if genome not in genomeDict:
        genomeDict[genome] = 0
        contigDict[genome] = list()
    genomeDict[genome] += leng
    contigDict[genome].append(line)

print 'Found %d genomes'%(len(genomeDict))

#drop header
cov.readline()
cov.readline()

covDict = dict()

for line in cov:
    #Get the sum coverage
    s = 0.0
    l = line.rstrip().split('\t')
    for i in range(1,len(l)):
        s += float(l[i])
    covDict[l[0]] = s

print 'Found %d genomes again. Hope they match'%(len(covDict))


perc.readline()
perc.readline()

percDict = dict()

for line in perc:
    #Get the sum coverage
    s = 0.0
    l = line.rstrip().split('\t')
    for i in range(1,len(l)):
        s += float(l[i])
    percDict[l[0]] = s

print 'Found %d genomes again. Hope they match'%(len(percDict))


table = []
#Get an approximation of how many reads hit each genome. This is as close as you will get to figuring out how long running it is going to take
for k in genomeDict.keys():
    read = genomeDict[k]*covDict[k]/100 #*percDict[k]/100
    table.append((k,read))


#Now, sort the table
t = sorted(table,key=operator.itemgetter(1),reverse=True)#must be True
#print(head(t))
#Great, now get these into X bins

res = [0]*nrToSplit
names = dict()

for k in range(len(t)):
    #Put this in the smallest one!
    pos = res.index(min(res))
    res[pos] += table[k][1]
    names[t[k][0]] = pos

#outf = sys.argv[5]
#Open as many files as bins
fileDict = dict()
for i in xrange(nrToSplit):
    fileDict[i] = open(outf+'_'+str(i),'w')

for k in names.keys():
    genomeName = k
    for c in contigDict[k]:
        fileDict[names[k]].write(c)
